ARG CENTOS_MAJOR=8
ARG POSTGRESQL_VERSION=13.6-0
ARG POSTGRESQL_MAJOR=13
FROM quay.io/centos/centos:$CENTOS_MAJOR as install
ARG POSTGRESQL_MAJOR
ARG RUM_VERSION=1.3.11

RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
RUN dnf -y group install "Development Tools"
RUN dnf -y module enable postgresql:$POSTGRESQL_MAJOR
RUN dnf -y install python3 libpq-devel postgresql-devel postgresql-contrib postgresql-server-devel systemtap-sdt-devel
RUN curl https://codeload.github.com/postgrespro/rum/tar.gz/refs/tags/$RUM_VERSION -o rum.tar.gz  \
    && tar -xzf rum.tar.gz && rm rum.tar.gz

ENV USE_PGXS=1
RUN cd rum-$RUM_VERSION && make && make install

FROM registry.developers.crunchydata.com/crunchydata/crunchy-postgres:centos${CENTOS_MAJOR}-${POSTGRESQL_VERSION}
ARG POSTGRESQL_MAJOR
COPY --from=install /usr/share/pgsql/extension/rum* /usr/pgsql-${POSTGRESQL_MAJOR}/share/extension/
COPY --from=install /usr/lib64/pgsql/rum* /usr/pgsql-${POSTGRESQL_MAJOR}/lib/
