# Crunchydata' pgo with RUM plugin

**Important Note:** This repository has been deprecated. It is no longer being actively maintained.

## Why is this repository deprecated?

This project repository is deprecated because it is no longer being actively used or maintained. While the code remains available for those who might find it useful, we will not be addressing issues or accepting pull requests.

## What happens to existing users?

Existing users of the project can continue to use the software but should be aware that it will not receive any further updates or security patches. It is highly recommended for users to migrate to another solution to avoid potential vulnerabilities.

We appreciate all the contributions and support from the community over the years.

Thank you for your understanding!

**Once again, This repository has now been marked as deprecated.**
